<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet"
	type="text/css" />
<title>列表页面配置列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
<table>
	<tr>
		<td colspan="8" class="tableHeadBg">
			<div align="center">列表页面配置列表</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td>网站ID</td>
		<td>网站名称</td>
		<td>所属搜索引擎ID</td>
		<td>所属搜索引擎名称</td>
		<td>日期模式</td>
		<td>最大记录数显示限制</td>
		<td>每页记录显示数量</td>
		<td>操作</td>
	</tr>
	<c:forEach items="${listPageConfigs}" var="listPageConfig" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${listPageConfig.websiteId }</td>
		<td>${listPageConfig.websiteName }</td>
		<td>${listPageConfig.searchEngineId }</td>
		<td>${listPageConfig.searchEngineName }</td>
		<td>${listPageConfig.jobDatePattern }</td>
		<td>${listPageConfig.maxRecordNum }</td>
		<td>${listPageConfig.pageSize }</td>
		<td><a href="${pageContext.request.contextPath}/admin/listpage/modify/${listPageConfig.id }">修改</a></td>
	</tr>
	</c:forEach>
</table>
<br>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
</body>
</html>
