<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>已经存在的数据日志表列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<ul id="tableList">
	<c:forEach var="table" items="${tables}">
	<li>
		<a href="<c:url value="/admin/spider/view"/>/${table}">${table}</a>
	</li>
	</c:forEach>
</ul>
</body>
</html>
