<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet"
	type="text/css" />
<title>搜索引擎列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
<table>
	<tr>
		<td colspan="14" class="tableHeadBg">
			<div align="center">搜索引擎列表</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td width="5%">网站ID</td>
		<td width="5%">网站名称</td>
		<td width="5%">搜索引擎ID</td>
		<td width="10%">搜索引擎名称</td>
		<td width="15%">查询URL生成类实现</td>
		<td width="10%">抓取前处理实现类</td>
		<td width="15%">抓取任务实现类</td>
		<td width="5%">每次抓取前休眠时间</td>
		<td width="25%" colspan="5">操作</td>
	</tr>
	<c:forEach items="${searchEngines}" var="searchEngine" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${searchEngine.websiteId }</td>
		<td>${searchEngine.websiteName }</td>
		<td>${searchEngine.id }</td>
		<td>${searchEngine.name }</td>
		<td class="warpword">${searchEngine.createQueryURLClass }</td>
		<td class="warpword">${searchEngine.beforeSpiderProcClass }</td>
		<td class="warpword">${searchEngine.spiderTaskClass }</td>
		<td>${searchEngine.sleepTime }</td>
		<td><a title="${searchEngine.name }" href="${pageContext.request.contextPath}/admin/engine/modify/${searchEngine.id }">修改引擎</a></td>
		<td><a title="${searchEngine.name }" href="${pageContext.request.contextPath}/admin/param/formByEngineId/${searchEngine.id }">添加参数</a></td>
		<td><a title="${searchEngine.name }" href="${pageContext.request.contextPath}/admin/param/engine/${searchEngine.id }">查看参数</a></td>
		<td><a title="${searchEngine.name }" href="${pageContext.request.contextPath}/admin/listpage/formByEngineId/${searchEngine.id }">添加解析</a></td>
		<td><a title="${searchEngine.name }" href="${pageContext.request.contextPath}/admin/listpage/modifyByEngineId/${searchEngine.id }">修改解析</a></td>
	</tr>
	</c:forEach>
</table>
<br>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
</body>
</html>
