package com.yzq.os.spider.v.service.domain;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yzq.os.spider.v.dao.LogRecordDao;
import com.yzq.os.spider.v.domain.LogRecord;
import com.yzq.os.spider.v.service.inter.OuterSystemService;

@Service
public class LogRecordService {
	@Autowired
	private LogRecordDao logRecordDao;

	@Autowired
	private OuterSystemService etlSystemService;

	public void save(LogRecord record) {
		logRecordDao.save(record);
	}

	public List<LogRecord> findBySpiderDate(Date crawlDate) {
		List<LogRecord> records = logRecordDao.findByCrawlDate(crawlDate);
		etlSystemService.setLogWebsiteNames(records);
		signNeedCheck(records);
		return records;
	}

	private List<LogRecord> signNeedCheck(List<LogRecord> records) {
		if (CollectionUtils.isNotEmpty(records)) {
			for (LogRecord record : records) {
				if (!DateUtils.isSameDay(record.getStartTime(), record.getFinishTime())) {
					record.setNeedCheck(true);
				}
			}
		}
		return records;
	}
}
