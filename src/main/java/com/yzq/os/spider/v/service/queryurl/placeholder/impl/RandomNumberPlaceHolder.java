package com.yzq.os.spider.v.service.queryurl.placeholder.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

/**
 * 随机数占位符
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class RandomNumberPlaceHolder extends AbstractPlaceHolder {

	private static final String PLACE_HOLDER_STRING = "<<RANDOM_NUM>>";

	private static final int NUM_LENGTH = 10;

	public RandomNumberPlaceHolder() {
		this.placeHolderString = PLACE_HOLDER_STRING;
	}

	@Override
	public String replace(String value) {
		String longStr = String.valueOf(RandomUtils.nextLong());
		longStr = StringUtils.substring(longStr, 0, NUM_LENGTH);
		return StringUtils.replace(value, getEncodedHolderString(), getEncodedHolderValue(longStr));
	}

	@Override
	public String returnPlaceString() {
		return this.placeHolderString;
	}

}
