package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.LogRecord;

public class LogRecordMapper implements RowMapper<LogRecord> {

	@Override
	public LogRecord mapRow(ResultSet rs, int index) throws SQLException {
		LogRecord record = new LogRecord();
		record.setId(rs.getInt("id"));
		record.setSpiderDate(rs.getDate("spider_date"));
		try {
			record.setWebsiteId(rs.getInt("site_id"));
		} catch (Exception e) {
		}
		record.setSearchEngineId(rs.getInt("search_engine_id"));
		try {
			record.setSearchEngineName(rs.getString("search_engine_name"));
		} catch (Exception e) {
		}
		record.setIp(rs.getString("ip"));
		record.setQueryCount(rs.getInt("query_count"));
		record.setQuerySpeed(rs.getInt("query_speed"));
		record.setRecordCount(rs.getInt("record_count"));
		record.setRecordSpeed(rs.getInt("record_count"));
		record.setStartTime(rs.getTimestamp("start_time"));
		record.setFinishTime(rs.getTimestamp("finish_time"));
		return record;
	}

}
