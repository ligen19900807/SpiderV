package com.yzq.os.spider.v.service.addition;

import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 执行登陆逻辑
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public interface Login {

	/**
	 * 设置网络访问服务类
	 * 
	 * @param httpClientService
	 */
	void setHttpClientService(HttpClientService httpClientService);

	/**
	 * 执行登陆逻辑
	 * 
	 * @throws Exception
	 */
	void login() throws Exception;

}
