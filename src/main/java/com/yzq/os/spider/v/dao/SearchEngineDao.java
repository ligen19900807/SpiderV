package com.yzq.os.spider.v.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.dao.mapper.SearchEngineMapper;
import com.yzq.os.spider.v.domain.SearchEngine;

@Repository
public class SearchEngineDao extends AbstractDao {

	public int save(final SearchEngine engine) {
		final StringBuffer sql = new StringBuffer();
		sql.append(" insert into search_engine ");
		sql.append("   (site_id, ");
		sql.append("    name, ");
		sql.append("    method, ");
		sql.append("    base_url, ");
		sql.append("    is_gzip, ");
		sql.append("    encode, ");
		sql.append("    url_encode, ");
		sql.append("    sleep_time, ");
		sql.append("    create_query_url_class, ");
		sql.append("    before_spider_processor_class, ");
		sql.append("    completion_spider_class, ");
		sql.append("    login_class, ");
		sql.append("    spider_task_class) ");
		sql.append(" values ");
		sql.append("   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplateOnline.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql.toString(), new String[] { "id" });
				ps.setInt(1, engine.getWebsiteId());
				ps.setString(2, engine.getName());
				ps.setInt(3, engine.getMethod());
				ps.setString(4, engine.getBaseUrl());
				ps.setInt(5, engine.getIsGzip());
				ps.setInt(6, engine.getEncode());
				ps.setInt(7, engine.getUrlEncode());
				ps.setInt(8, engine.getSleepTime());
				ps.setString(9, engine.getCreateQueryURLClass());
				ps.setString(10, engine.getBeforeSpiderProcClass());
				ps.setString(11, engine.getCompletionSpiderClass());
				ps.setString(12, engine.getLoginClass());
				ps.setString(13, engine.getSpiderTaskClass());
				return ps;
			}
		}, keyHolder);

		return keyHolder.getKey().intValue();
	}


	public List<SearchEngine> findAllList() {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.site_id, ");
		sql.append("        t.name, ");
		sql.append("        t.method, ");
		sql.append("        t.base_url, ");
		sql.append("        t.is_gzip, ");
		sql.append("        t.encode, ");
		sql.append("        t.url_encode, ");
		sql.append("        t.sleep_time,");
		sql.append("        t.create_query_url_class, ");
		sql.append("        t.before_spider_processor_class, ");
		sql.append("        t.completion_spider_class, ");
		sql.append("        t.login_class, ");
		sql.append("        t.spider_task_class ");
		sql.append("   from search_engine t ");

		return jdbcTemplateOnline.query(sql.toString(), new SearchEngineMapper());
	}


	public SearchEngine findById(int engineId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.site_id, ");
		sql.append("        t.name, ");
		sql.append("        t.method, ");
		sql.append("        t.base_url, ");
		sql.append("        t.is_gzip, ");
		sql.append("        t.encode, ");
		sql.append("        t.url_encode, ");
		sql.append("        t.sleep_time,");
		sql.append("        t.create_query_url_class, ");
		sql.append("        t.before_spider_processor_class, ");
		sql.append("        t.completion_spider_class, ");
		sql.append("        t.login_class, ");
		sql.append("        t.spider_task_class ");
		sql.append("   from search_engine t ");
		sql.append("  where t.id = ? ");

		return jdbcTemplateOnline.queryForObject(sql.toString(), new Object[] { engineId }, new SearchEngineMapper());
	}

	public int countById(int engineId) {
		String sql = " select count(*) as cnt from search_engine where id = ? ";
		return jdbcTemplateOnline.queryForInt(sql, engineId);
	}

	public void update(SearchEngine engine) {
		StringBuffer sql = new StringBuffer();
		sql.append(" update search_engine t ");
		sql.append("    set t.site_id                    = ?, ");
		sql.append("        t.name                                 = ?, ");
		sql.append("        t.method                              = ?, ");
		sql.append("        t.base_url                             = ?, ");
		sql.append("        t.is_gzip                                = ?, ");
		sql.append("        t.encode                                = ?, ");
		sql.append("        t.url_encode                           = ?, ");
		sql.append("        t.sleep_time                            = ?,");
		sql.append("        t.create_query_url_class           = ?, ");
		sql.append("        t.before_spider_processor_class = ?, ");
		sql.append("        t.completion_spider_class          = ?, ");
		sql.append("        t.login_class                             = ?, ");
		sql.append("        t.spider_task_class                    = ? ");
		sql.append("  where t.id = ? ");

		List<Object> params = new ArrayList<Object>();
		params.add(engine.getWebsiteId());
		params.add(engine.getName());
		params.add(engine.getMethod());
		params.add(engine.getBaseUrl());
		params.add(engine.getIsGzip());
		params.add(engine.getEncode());
		params.add(engine.getUrlEncode());
		params.add(engine.getSleepTime());
		params.add(engine.getCreateQueryURLClass());
		params.add(engine.getBeforeSpiderProcClass());
		params.add(engine.getCompletionSpiderClass());
		params.add(engine.getLoginClass());
		params.add(engine.getSpiderTaskClass());
		params.add(engine.getId());

		jdbcTemplateOnline.update(sql.toString(), params.toArray());

	}

}
