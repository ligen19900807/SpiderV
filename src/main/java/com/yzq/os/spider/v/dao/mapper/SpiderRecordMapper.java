package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.Record;

public class SpiderRecordMapper implements RowMapper<Record> {

	@Override
	public Record mapRow(ResultSet rs, int index) throws SQLException {
		Record job = new Record();
		job.setId(rs.getInt("job_id"));
		job.setWebsiteId(rs.getInt("website_id"));
		job.setSearchEngineId(rs.getInt("search_engine_id"));
		job.setQueryUrlId(rs.getInt("query_url_id"));
		job.setJobTitle(rs.getString("job_title"));
		job.setCompanyName(rs.getString("company_name"));
		job.setCmpCompanyId(rs.getString("cmp_company_id"));
		job.setCityText(rs.getString("city_text"));
		job.setJobDate(rs.getDate("job_date"));
		job.setJobTypeCode(rs.getString("job_type_code"));
		job.setIndustryCode(rs.getString("job_industry_code"));
		job.setJobLinkURL(rs.getString("job_link_url"));
		job.setCompanyLinkURL(rs.getString("company_link_url"));
		job.setCityCode(rs.getString("job_city_code"));
		job.setDoFlag(rs.getInt("do_flag"));
		job.setCmptrJobId(rs.getString("cmptr_job_id"));
		job.setUniqueMd5(rs.getString("unique_md5"));
		job.setJobSalary(rs.getString("job_salary"));
		job.setJobType(rs.getString("job_type"));
		return job;
	}

}
