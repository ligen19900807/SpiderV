package com.yzq.os.spider.v.service.job;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.yzq.os.spider.v.Constants;
import com.yzq.os.spider.v.domain.SpiderTask;
import com.yzq.os.spider.v.service.domain.SpiderRecordService;

/**
 * 抓取任务执行
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class SpiderRecordDetail extends QuartzJobBean implements StatefulJob {

	@Override
	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
		SpiderRecordService spiderRecordService = Constants.getApplicationContext().getBean("crawlJobService", SpiderRecordService.class);
		SpiderTask task = (SpiderTask) ctx.getJobDetail().getJobDataMap().get(SchedulerService.TASK_DATA_MAP_KEY);
		spiderRecordService.callSpider(SpiderRecordService.AUTOMATIC_CALL, task.getSearchEngineId(), new Date(), task.getFindUrlSize(), task.getMinThreadNum(), task.getMaxThreadNum(), task.getPoolQueueSize());
	}
}
