package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.QueryURL;

public class QueryURLMapper implements RowMapper<QueryURL> {

	@Override
	public QueryURL mapRow(ResultSet rs, int index) throws SQLException {
		QueryURL url = new QueryURL();
		url.setId(rs.getInt("id"));
		url.setSearchEngineId(rs.getInt("search_engine_id"));
		url.setSpellUrl(rs.getString("spell_url"));
		url.setPostUrl(rs.getString("post_url"));
		url.setDoFlag(rs.getInt("do_flag"));
		return url;
	}

}
