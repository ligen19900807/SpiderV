package com.yzq.os.spider.v.domain;

import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;

/**
 * 系统抓取记录
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class LogRecord {
	private Integer id;
	/**
	 * 执行抓取日期
	 */
	private Date spiderDate;
	
	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;
	
	/**
	 * 执行抓取的集群服务器IP地址
	 */
	private String ip;
	
	/**
	 * 提交搜索URL总量
	 */
	private int queryCount;
	
	/**
	 *提交搜索URL速度 
	 */
	private int querySpeed;
	
	/**
	 * 抓取记录总数量
	 */
	private int recordCount;
	
	/**
	 * 抓取记录速度
	 */
	private int recordSpeed;
	
	/**
	 * 抓取任务开始时间
	 */
	private Date startTime;
	
	/**
	 * 抓取任务结束时间
	 */
	private Date finishTime;
	
	/**
	 * 是否需要执行人工检查
	 */
	private boolean needCheck;

	/**
	 * 网站ID
	 */
	private Integer websiteId;
	
	/**
	 *网站名称 
	 */
	private String websiteName;
	
	/**
	 * 搜索引擎名称
	 */
	private String searchEngineName;

	/**
	 * 数据库保存记录总数量
	 */
	private Integer savedJobCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public Date getSpiderDate() {
		return spiderDate;
	}

	public void setSpiderDate(Date spiderDate) {
		this.spiderDate = spiderDate;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getQueryCount() {
		return queryCount;
	}

	public void setQueryCount(int queryCount) {
		this.queryCount = queryCount;
	}

	public int getQuerySpeed() {
		return querySpeed;
	}

	public void setQuerySpeed(int querySpeed) {
		this.querySpeed = querySpeed;
	}

	

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public int getRecordSpeed() {
		return recordSpeed;
	}

	public void setRecordSpeed(int recordSpeed) {
		this.recordSpeed = recordSpeed;
	}

	public Integer getWebsiteId() {
		return websiteId;
	}

	public void setWebsiteId(Integer websiteId) {
		this.websiteId = websiteId;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	
	public String getSearchEngineName() {
		return searchEngineName;
	}

	public void setSearchEngineName(String searchEngineName) {
		this.searchEngineName = searchEngineName;
	}

	public Integer getSavedJobCount() {
		return savedJobCount;
	}

	public void setSavedJobCount(Integer savedJobCount) {
		this.savedJobCount = savedJobCount;
	}

	public boolean isNeedCheck() {
		return needCheck;
	}

	public void setNeedCheck(boolean needCheck) {
		this.needCheck = needCheck;
	}

	@Override
	public String toString() {
		return "LogRecord [id=" + id + ", crawlDate=" + DateFormatUtils.format(spiderDate, "yyyy-MM-dd") + ", searchEngineId=" + searchEngineId + ", ip=" + ip + ", queryCount=" + queryCount + ", querySpeed=" + querySpeed + ", recordCount=" + recordCount + ", recordSpeed=" + recordSpeed + ", finishTime=" + DateFormatUtils.format(finishTime, "yyyy-MM-dd HH:mm:ss") + "]";
	}

}
