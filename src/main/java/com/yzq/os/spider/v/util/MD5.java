package com.yzq.os.spider.v.util;

import java.security.MessageDigest;

/**
 * MD5工具类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public final class MD5 {
	
	private MD5(){}
	
	/**
	 * 获取指定字符串的MD5
	 * @param input
	 * @param charsetName
	 * @return
	 */
	public static String Md5(String input, String charsetName) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] strTemp = null;
			if (charsetName != null && charsetName.length() > 0) {
				strTemp = input.getBytes(charsetName);
			} else {
				strTemp = input.getBytes();
			}
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}

}
