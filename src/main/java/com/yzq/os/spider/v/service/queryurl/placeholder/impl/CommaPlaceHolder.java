package com.yzq.os.spider.v.service.queryurl.placeholder.impl;

import org.apache.commons.lang.StringUtils;

/**
 * 逗号占位符
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class CommaPlaceHolder extends AbstractPlaceHolder {

	private static final String PLACE_HOLDER_STRING = "<<COMMA>>";

	public CommaPlaceHolder() {
		this.placeHolderString = PLACE_HOLDER_STRING;
	}

	@Override
	public String replace(String value) {
		return StringUtils.replace(value, getEncodedHolderString(), getEncodedHolderValue(","));
	}

	@Override
	public String returnPlaceString() {
		return this.placeHolderString;
	}
}
