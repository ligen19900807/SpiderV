package com.yzq.os.spider.v.service.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yzq.os.spider.v.dao.ListPageConfigDao;
import com.yzq.os.spider.v.domain.ListPageConfig;
import com.yzq.os.spider.v.service.inter.OuterSystemService;

@Service
public class ListPageConfigService {
	@Autowired
	private ListPageConfigDao listPageConfigDao;

	@Autowired
	private OuterSystemService etlSystemService;

	public void save(ListPageConfig listPageConfig) {
		listPageConfigDao.save(listPageConfig);
	}

	public List<ListPageConfig> findAll() {
		List<ListPageConfig> configs = listPageConfigDao.findAll();
		etlSystemService.setListPageWebsiteNames(configs);
		return configs;
	}

	public ListPageConfig findById(int id) {
		return listPageConfigDao.findById(id);
	}

	public ListPageConfig findBySearchEngineId(int searchEngineId) {
		return listPageConfigDao.findBySearchEngineId(searchEngineId);
	}

	public void update(ListPageConfig listPageConfig) {
		listPageConfigDao.update(listPageConfig);
	}

	public boolean isExist(int searchEngineId) {
		ListPageConfig config = listPageConfigDao.findBySearchEngineId(searchEngineId);
		if (config != null) {
			return true;
		} else {
			return false;
		}
	}

	public void deleteBySearchEngineId(Integer searchEngineId) {
		listPageConfigDao.deleteBySearchEngineId(searchEngineId);
	}
}
