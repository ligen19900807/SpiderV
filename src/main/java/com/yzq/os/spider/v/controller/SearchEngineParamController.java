package com.yzq.os.spider.v.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.yzq.os.spider.v.domain.SearchEngine;
import com.yzq.os.spider.v.domain.SearchEngineParam;
import com.yzq.os.spider.v.service.domain.SearchEngineParamService;
import com.yzq.os.spider.v.service.domain.SearchEngineService;

/**
 * 搜索引擎参数控制器
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Controller
@RequestMapping("/param")
public class SearchEngineParamController extends CommonController {

	@Autowired
	private SearchEngineService searchEngineService;

	@Autowired
	private SearchEngineParamService searchEngineParamService;

	/**
	 * 添加搜索引擎参数
	 * 
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView form() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		model.put("placeHolders", searchEngineParamService.getplaceholdersUI());
		return new ModelAndView("/admin/param/form", model);
	}

	/**
	 * 通过搜索引擎ID查找搜索引擎参数
	 * 
	 * @param engineId
	 * @return
	 */
	@RequestMapping("/formByEngineId/{engineId}")
	public ModelAndView formByEngineId(@PathVariable int engineId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		model.put("placeHolders", searchEngineParamService.getplaceholdersUI());
		SearchEngineParam searchEngineParam = new SearchEngineParam();
		searchEngineParam.setSearchEngineId(engineId);
		model.put("searchEngineParam", searchEngineParam);
		return new ModelAndView("/admin/param/form", model);
	}

	/**
	 * 保存搜索引擎参数
	 * 
	 * @param ep
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public RedirectView save(SearchEngineParam ep) {
		if (ep.getId() == null) {
			searchEngineParamService.save(ep);
		} else {
			searchEngineParamService.update(ep);
		}
		return new RedirectView("param/form");
	}

	/**
	 * 修改搜索引擎参数
	 * 
	 * @param epId
	 * @return
	 */
	@RequestMapping("/modify/{epId}")
	public ModelAndView modify(@PathVariable int epId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		model.put("placeHolders", searchEngineParamService.getplaceholdersUI());
		model.put("searchEngineParam", searchEngineParamService.findById(epId));
		return new ModelAndView("/admin/param/form", model);
	}

	/**
	 * 通过搜索引擎ID查找搜索引擎参数
	 * 
	 * @param eId
	 * @return
	 */
	@RequestMapping("/engine/{eId}")
	public ModelAndView find(@PathVariable int eId) {
		Map<String, Object> model = new HashMap<String, Object>();
		SearchEngine searchEngine = searchEngineService.findById(eId);
		model.put("searchEngineParams",
				searchEngineParamService.findBySearchEngineId(eId));
		model.put("searchEngine", searchEngine);
		return new ModelAndView("/admin/param/list", model);
	}
}
