package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.Server;

public class ServerMapper implements RowMapper<Server> {

	@Override
	public Server mapRow(ResultSet rs, int index) throws SQLException {
		Server server = new Server();
		server.setId(rs.getInt("id"));
		server.setIp(rs.getString("ip"));
		server.setType(rs.getInt("type"));
		return server;
	}

}
