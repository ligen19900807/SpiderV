package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.SearchEngine;

public class SearchEngineMapper implements RowMapper<SearchEngine> {

	@Override
	public SearchEngine mapRow(ResultSet rs, int index) throws SQLException {
		SearchEngine engine = new SearchEngine();
		engine.setId(rs.getInt("id"));
		engine.setWebsiteId(rs.getInt("site_id"));
		engine.setName(rs.getString("name"));
		engine.setMethod(rs.getInt("method"));
		engine.setBaseUrl(rs.getString("base_url"));
		engine.setIsGzip(rs.getInt("is_gzip"));
		engine.setEncode(rs.getInt("encode"));
		engine.setUrlEncode(rs.getInt("url_encode"));
		engine.setSleepTime(rs.getInt("sleep_time"));
		engine.setCreateQueryURLClass(rs.getString("create_query_url_class"));
		engine.setBeforeSpiderProcClass(rs.getString("before_spider_processor_class"));
		engine.setCompletionSpiderClass(rs.getString("completion_spider_class"));
		engine.setLoginClass(rs.getString("login_class"));
		engine.setSpiderTaskClass(rs.getString("spider_task_class"));
		return engine;
	}

}
