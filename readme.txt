简介：
	应用用来抓取垂直网站数据，系统经过简单配置即可完成典型的“条件选择”--》“搜索”--》“列表数据”这总结构的数据抓取，抽取部分通过JSOUP自己写一个实现即可。实现类要求放到（com/yzq/os/spider/v/service/spider/impl）
	系统支持集群模式，所有服务器代码统一，可自动化同步管理，通过数据库中的配置标识唯一主机，主机负责一些日常初始化及清理动作。每台服务器可以分别设定自动抓取任务。
	系统根据搜索参数配置自动生成搜索URL组合，并可以通过一次运行后，将有结果数据的搜索URL备份，下次从备份URL中取出运行，减少不必要的搜索条件提交；
	手动运行一次如果正常后可以设定定时任务，每天定时运行；
	系统采用maven管理，应用采用UTF-8编码。

软件要求：
	linux jdk 1.6 tomcat6+ mysql 5.5+ （InnoDB引擎，数据库编码UTF-8） 

安装部署及运行：
	1、首先下载项目源代码；
	2、根据自己情况修改properties文件；
	3、运行maven打包war；
	4、部署到tomcat webapps目录下
	5、创建mysql账号，并执行/scripts/create_database.sql 和/scripts/init.sql（需要根据自身情况修改）
	6、分析要抓取的网站。可以通过WebSiteCrawlTest类来进行（收集配置数据）
	7、配置搜索引擎、搜索引擎参数、列表页面配置
	8、可选择实现数据抽取类，参考com.yzq.os.spider.v.service.spider.impl.DemoCrawlTask
	9、初始化搜索URL参数http://localhost:8080/SpiderVertical/admin/createurl/form
	10、执行抓取http://localhost:8080/SpiderVertical/admin/spider/form
	11、查看运行进度http://localhost:8080/SpiderVertical/admin/statis
	12、运行完查看数据结果http://localhost:8080/SpiderVertical/admin/spider/view_tables
	
注意：使用系统请遵守Robots协议
有问题请发送mail到:xingyu_yzq@163.com
